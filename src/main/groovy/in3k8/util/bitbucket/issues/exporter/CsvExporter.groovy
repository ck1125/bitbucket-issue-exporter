package in3k8.util.bitbucket.issues.exporter

import in3k8.util.bitbucket.model.Issue
import java.text.SimpleDateFormat
import java.util.regex.Pattern


class CsvExporter implements Exporter {

    private static final Pattern ESTIMATE_PATTERN = Pattern.compile(/##(.*)Estimate:(.*)((\d{1,2})d)(.*)##/)

    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy")

    void export(List<Issue> issues, String file) {

        new FileWriter(file).withWriter {Writer oWriter ->
            printHeader(oWriter)
            printBody(oWriter, issues)

        }

    }

    private printBody(Writer writer, List<Issue> issues) {
        println issues.size()
        issues.each { Issue issue ->
            String formattedDate = ''
            if (issue.lastUpdateTime) {
                formattedDate = formatter.format(issue.lastUpdateTime)
            }

            writer.write("""${issue.title}","${parseContent(issue.description)}","${issue.status}","${issue.version}","${formattedDate}","${getEstimate(issue.description)}"\n""")
        }
    }

    String getEstimate(String description) {
        return ESTIMATE_PATTERN.matcher(description)[0][4]
    }

    private void printHeader(Writer writer) {
        writer.write('Title,Description,Status,Version,Last Updated,"Estimate in Days"\n')

    }

    private String parseContent(String content) {

        String sansEstimate = content.replaceAll('##(.*)Estimate:(.*)##', '')
        return sansEstimate
    }


}
