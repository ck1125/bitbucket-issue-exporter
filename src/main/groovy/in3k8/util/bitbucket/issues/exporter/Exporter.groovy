package in3k8.util.bitbucket.issues.exporter

import in3k8.util.bitbucket.model.Issue


interface Exporter {

    void export(List<Issue> issues, String file)
}
