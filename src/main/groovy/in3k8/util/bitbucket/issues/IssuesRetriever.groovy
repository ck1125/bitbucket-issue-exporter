package in3k8.util.bitbucket.issues

import in3k8.util.bitbucket.model.Issue
import in3k8.util.bitbucket.services.BitbucketService
import in3k8.util.bitbucket.issues.exporter.Exporter
import in3k8.util.bitbucket.issues.exporter.CsvExporter

class IssuesRetriever {


    BitbucketService bitbucketService
    Exporter exporter

    static void main(String[] args) {
        println args
        IssuesRetriever retriever = new IssuesRetriever(exporter: new CsvExporter(),bitbucketService: new BitbucketService(args[0],args[1]))
        retriever.exportEstimatedIssues(args[2],args[3])
    }


    void exportEstimatedIssues(String repoId, String fileName) {
        List<Issue> issues = findEstimatedIssues(repoId)
        exporter.export(issues,fileName)
    }

    List<Issue> findEstimatedIssues(String repoId) {
        return bitbucketService.findIssues(repoId,[content: '~Estimate:',version:'1.0',status:'new',kind:'enhancement',limit:'50'])
    }



}
