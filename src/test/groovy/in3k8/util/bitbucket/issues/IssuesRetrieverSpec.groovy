package in3k8.util.bitbucket.issues;


import spock.lang.Specification
import in3k8.util.bitbucket.model.Issue
import in3k8.util.bitbucket.services.BitbucketService
import in3k8.util.bitbucket.issues.exporter.Exporter
import in3k8.util.bitbucket.issues.IssuesRetriever

public class IssuesRetrieverSpec extends Specification {

    IssuesRetriever retriever
    BitbucketService mockBitbucketService
    Exporter mockExporter

    void setup() {
        retriever = new IssuesRetriever()
        mockBitbucketService = Mock(BitbucketService)
        retriever.bitbucketService = mockBitbucketService

        mockExporter = Mock(Exporter)
        retriever.exporter = mockExporter
    }

    def 'retrieve estimated issues'() {
        given:
            List<Issue> expected = []
        when:
            List<Issue> actual = retriever.findEstimatedIssues('repoId')

        then:
            1 * mockBitbucketService.findIssues('repoId',[content:'~Estimate:',version:'1.0',status:'new',kind:'enhancement',limit:'50']) >> expected
            0 * _
        and:
            actual == expected

    }


    def 'export estimated issues'() {

        given:
            List<Issue> expected = [new Issue(status: 'open',title: 'hi',description: 'hello')]

        when:
            retriever.exportEstimatedIssues('repoId','fileName')

        then:
            1 * mockBitbucketService.findIssues('repoId',[content:'~Estimate:',version:'1.0',status:'new',kind:'enhancement',limit:'50']) >> expected
            1 * mockExporter.export(expected,'fileName')
            0 * _


    }

}
